/**
 * Created by pawelbryniarski on 02.04.16.
 */
public class InvestmentCalculator {

    Investment findMostProfitableInvestment(int[] plotsProfits) {
        Investment currentInvestment = new Investment(0, 0, plotsProfits[0]);
        Investment maxValueInvestment = new Investment(0, 0, plotsProfits[0]);

        for (int i = 1; i < plotsProfits.length; i++) {

            if (currentInvestment.getValue() <= 0) {
                currentInvestment = new Investment(i, i, plotsProfits[i]);
            } else {
                currentInvestment = new Investment(currentInvestment.getStart(), i, currentInvestment.getValue() + plotsProfits[i]);
            }

            if (currentInvestment.compareTo(maxValueInvestment) > 0) {
                maxValueInvestment = new Investment(currentInvestment);
            }
        }
        return maxValueInvestment;
    }
}
