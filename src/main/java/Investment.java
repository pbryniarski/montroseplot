/**
 * Created by pawelbryniarski on 02.04.16.
 */
public class Investment implements Comparable {

    private final int start;
    private final int end;
    private final int value;

    public Investment(int start, int end, int value) {
        this.start = start;
        this.end = end;
        this.value = value;
    }

    public Investment(Investment investment) {
        this.start = investment.getStart();
        this.end = investment.getEnd();
        this.value = investment.getValue();
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public int getValue() {
        return value;
    }

    @Override
    public int compareTo(Object o) {
        Investment investment = (Investment)o;
        int valueComparison = Integer.valueOf(value).compareTo(investment.value);

        if(valueComparison != 0){
            return valueComparison;
        }

        int lengthComparison = Integer.valueOf(end - start).compareTo(investment.end - investment.start);

        // greater is worse, we look for shorter list of plots
        return -1 * lengthComparison;
    }

    @Override
    public String toString() {
        return "" + (start + 1) + " " + (end + 1) + " " + value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Investment that = (Investment) o;

        if (start != that.start) return false;
        if (end != that.end) return false;
        return value == that.value;

    }

    @Override
    public int hashCode() {
        int result = start;
        result = 31 * result + end;
        result = 31 * result + value;
        return result;
    }
}
