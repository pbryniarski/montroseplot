/**
 * Created by pawelbryniarski on 02.04.16.
 */
public class InputParser {

    public int[] parse(String input){
        String [] items = input.split("\\s+");
        int[] values = new int[items.length];
        for (int i = 0; i < items.length; i++) {
            values[i] = Integer.valueOf(items[i]);
        }
        return values;
    }
}
