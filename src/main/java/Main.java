/**
 * Created by pawelbryniarski on 31.03.16.
 */
public class Main {

    public static void main(String[] args) {
        InputReader reader = new InputReader(System.in);
        InputParser parser = new InputParser();
        InvestmentCalculator calculator = new InvestmentCalculator();

        int[] plotsProfits = parser.parse(reader.read());
        Investment maxValueInvestment = calculator.findMostProfitableInvestment(plotsProfits);

        System.out.println(maxValueInvestment);
    }
}