import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by pawelbryniarski on 02.04.16.
 */
public class InputReader {

    private final InputStream inputStream;

    public InputReader(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String read() {
        // ignore
        String nrOfItems = null;
        String items;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            nrOfItems = br.readLine();
            items = br.readLine();
        } catch (IOException io) {
            io.printStackTrace();
            return null;
        }

        return items;
    }
}
