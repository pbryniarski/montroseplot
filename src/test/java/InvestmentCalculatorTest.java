import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by pawelbryniarski on 03.04.16.
 */
@RunWith(Parameterized.class)
public class InvestmentCalculatorTest {

    private final int[] input;
    private final Investment output;
    private InvestmentCalculator investmentCalculator;

    public InvestmentCalculatorTest(int[] input, Investment output) {
        this.input = input;
        this.output = output;
    }

    @Parameterized.Parameters
    public static Collection inputsOutputs() {
        return Arrays.asList(new Object[][]{
                {new int[]{1, 2, 3, 4, 5}, new Investment(0, 4, 15)},
                {new int[]{0, 0, 0, -10, 12, -5, 6, -1}, new Investment(4, 6, 13)},
                {new int[]{7, -1, -1, -6, 2, 6}, new Investment(4, 5, 8)},
                {new int[]{-1, -1, -1, 1, 0, 0, 0, 0, -2}, new Investment(3, 3, 1)},
                {new int[]{1, 1, -5, 1, 1}, new Investment(0, 1, 2)},
                {new int[]{1,0, 1, -5, 1, 1}, new Investment(4, 5, 2)},
                {new int[]{-5, 0, 7, -6, 4, 3, -5, 0, 2}, new Investment(2,5,8)},
                {new int[]{1}, new Investment(0, 0, 1)}
        });
    }

    @Before
    public void setUp() throws Exception {
        investmentCalculator = new InvestmentCalculator();
    }

    @Test
    public void testCalculate() throws Exception {
        Investment calculatedMax = investmentCalculator.findMostProfitableInvestment(input);
        assertEquals(output, calculatedMax);
    }
}