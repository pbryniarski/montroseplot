import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by pawelbryniarski on 03.04.16.
 */
@RunWith(Parameterized.class)
public class InputParserTest {

    private InputParser inputParser;

    private final String input;
    private final int[] output;

    public InputParserTest(String input, int[] output) {
        this.input = input;
        this.output = output;
    }

    @Parameterized.Parameters
    public static Collection inputsOutputs() {
        return Arrays.asList(new Object[][]{
                {"1 2 3 4 5 6 7 -1 2 -3 -4 0 0 0 0", new int[]{1, 2, 3, 4, 5, 6, 7, -1, 2, -3, -4, 0, 0, 0, 0}},
                {"1 1 1 1 1 -123456", new int[]{1, 1, 1, 1, 1, -123456}}
        });
    }

    @Before
    public void setUp() throws Exception {
        inputParser = new InputParser();

    }

    @Test
    public void testParse() throws Exception {
        int[] parsed = inputParser.parse(input);
        assertEquals(output.length, parsed.length);
        for (int i = 0; i < output.length; i++) {
            assertEquals(output[i], parsed[i]);
        }
    }
}